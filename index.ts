/* eslint-disable @typescript-eslint/no-explicit-any */
import express, { Response, Request,NextFunction } from 'express'
import morgan from 'morgan'
import log from '@ajar/marker'
import cors from 'cors'

import {connect_db} from './db/mongoose.connection.js';
import user_router from './modules/user/user.router.js';
import song_router from './modules/song/song.router.js';
import artist_router from './modules/artist/artist.router.js';
import playlist_router from './modules/playlist/playlist.router.js';
import auth_router from  './modules/auth/auth.router.js';

import {errorHandler,not_found} from './middleware/errors.handler.js';   


class App {
  app: any;
  
  constructor() {
    
    this.app = express();
    this.applyGlobalMiddleware()
    this.applyRouters()
    this.applyErrorHandlers()
    this.startServer()

  }
  applyGlobalMiddleware(){
     // middleware
    this.app.use(cors());
    this.app.use(morgan('dev'))
    this.app.use((req:Request,res:Response,Next:NextFunction)=>{
      console.log("before");
      Next();
    })
  }
  
  applyRouters(){
    this.app.use('/api/auth', auth_router);
    this.app.use('/api/users', user_router);
    this.app.use('/api/songs', song_router);
    this.app.use('/api/playlists', playlist_router);
    this.app.use('/api/artists', artist_router);
    this.app.use('*', not_found)

  }

  applyErrorHandlers(){
    this.app.use((req:Request,res:Response,Next:NextFunction)=>{
      console.log("AFTER");
      Next();
    })
    this.app.use(errorHandler);    
  }
  async startServer() {
    const { PORT=8080 ,HOST="localhost" } = process.env;

    await this.app.listen(PORT,HOST);
    log.magenta(`api is live on`,` ✨ ⚡  http://${HOST}:${PORT} ✨ ⚡`);  
  }
}

const myServerInstance = new App();


