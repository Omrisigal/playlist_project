import express, {Response, Request, NextFunction} from 'express';

export async function addReqID (req : Request,res: Response,next: NextFunction) {
    req.id = Math.random().toString(36).substring(7);
    next();
}
