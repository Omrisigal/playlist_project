import express, {Response, Request, NextFunction} from 'express';
import jwt from 'jsonwebtoken';
const {APP_SECRET = "supercalifragilisticexpialidocious"} = process.env;
import { ROLE } from '../constants.js';


export async function authenticate (req : Request,res: Response, next : NextFunction) {
    const access_token = req.headers['x-access-token'] as string | undefined;
    if (!access_token) return res.status(403).json({
        status:'Unauthorized',
        payload: 'No token provided.'
    });

    // verifies secret and checks exp
    try{
        const decoded = await jwt.verify(access_token, APP_SECRET);
        console.log({decoded})
        
        //check user refresh token in DB
        const {user_id,user_roles} = decoded;
        req.user_id = user_id;
        req.user_roles = user_roles;
        next();

    }
    catch(err){
        next(err)
    }
    // req.user_id = decoded.id;
}

export async function authorize (roles: ROLE[]){
    return async function(req : Request,res: Response, next : NextFunction) {
        const user_roles = req.user_roles;
        const user_rols_arr = user_roles.split(",");
        // const intersection = user_rols_arr.filter(x => roles.includes(x));
        let flag = false;
        roles.forEach(role=>{
            if(user_rols_arr.includes(role.toString())){
                flag = true;
            }
        })
        if(flag) {
            next()
        }
        else{
            return res.status(403).json({
                status:'Unauthorized',
                payload: 'Not authorized for using the resource.'
            });
        }
    }
}