import user_model,{IUser} from "../../modules/user/user.model.js";
import song_model from "../../modules/song/song.model.js";
import artist_model from "../../modules/artist/artist.model.js";
import playlist_model from "../../modules/playlist/playlist.model.js";
import { SongSchema } from "../../modules/song/song.model.js";
import { ObjectId, Schema } from "mongoose";
import { ICreateArtistDto } from "../../modules/artist/artist.interface.js";
import SONG_DAL from "./mongo_song_dal.js";

class ARTIST_DAL{
        
        async getAllArtists(){
            return await artist_model.find();
        }
        async getArtistByID(id: string ) {
            return await artist_model.findById(id);
        }

        async deleteByID(id: string){
            const artist = await this.getArtistByID(id);
            for (const song_id of artist.songs) {
                SONG_DAL.deleteByID(song_id);
            }
            return await artist_model.findByIdAndDelete(id);
        }
        async create(data:ICreateArtistDto){
            return await artist_model.create(data);
        }
        async getArtistSongsByID(id:string) {
            return await artist_model.findById(id).populate('songs');

        }
        async deleteSongFromArtist(artist_id: string,song_id: string){
            const artist = await this.getArtistByID(artist_id);
            const song_index = artist.songs.findIndex((song:any)=>song._id === song_id);
            artist.songs.splice(song_index,1);
            await artist.songs.save();
        }
        
        
}
export default new ARTIST_DAL();

