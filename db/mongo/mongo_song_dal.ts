import user_model,{IUser} from "../../modules/user/user.model.js";
import song_model, { ISong } from "../../modules/song/song.model.js";
import artist_model from "../../modules/artist/artist.model.js";
import playlist_model from "../../modules/playlist/playlist.model.js";
import songModel, { SongSchema } from "../../modules/song/song.model.js";
import { ObjectId, Schema } from "mongoose";
import PLAYLIST_DAL from "./mongo_playlist_dal.js";
import ARTIST_DAL from "./mongo_artist_dal.js";
import USER_DAL from "./mongo_user_dal.js";

import { ICreateSongDto } from "../../modules/song/song.interface.js";


class SONG_DAL{
        
        async getAllSongs(){
            return await song_model.find();
        }
        async getSongByID(id: string) {
            return await song_model.findById(id);
        }

        async create(data:ICreateSongDto){
            const song = await song_model.create(data);
            const artist = await ARTIST_DAL.getArtistByID(data.artist);
            // const artist = await artist_model.findById(data.artist);
            artist.songs.push(song._id);
            await artist.save();
            return song;
        }

        async addSongToPlaylist(song_id: string, playlist_id: string){
            const song = await this.getSongByID(song_id);
            const playlist = await PLAYLIST_DAL.getPlaylistByID(playlist_id);
            if(song){
                song.playlists.push(playlist);
                await song.save();
            }
            const added_playlist = await PLAYLIST_DAL.addSongToPlaylist(playlist_id, song_id);
            return song;
        }

        async removePlaylistFromSong(song_id: string, playlist_id: string){
            const song = await song_model.findById(song_id);
            if (song){
                const playlist_index = song.playlists.findIndex((playlist:any)=>playlist._id === playlist_id);
                song.playlists.splice(playlist_index,1);
                await song.save();
            }
            return song;
        }

        async deleteByID(id: string){
            const song = await this.getSongByID(id);
            if(song){
                const artist_id = song.artist;
                await  ARTIST_DAL.deleteSongFromArtist(artist_id.toString(), id)
                for (const playlist_id of song.playlists) {
                    PLAYLIST_DAL.deleteSongFromPlaylist(playlist_id.toString(), id);
                }
            }
            return await song_model.findByIdAndDelete(id);
        }
        
       
}
export default new SONG_DAL();

