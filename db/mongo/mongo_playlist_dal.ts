import user_model,{IUser} from "../../modules/user/user.model.js";
import song_model, { ISong } from "../../modules/song/song.model.js";
import artist_model from "../../modules/artist/artist.model.js";
import playlist_model, { IPlaylist } from "../../modules/playlist/playlist.model.js";
import { SongSchema } from "../../modules/song/song.model.js";
import { Schema } from "mongoose";
import SONG_DAL from "./mongo_song_dal.js";
import { ICreatePlaylistDto } from "../../modules/playlist/playlist.interface.js";
import USER_DAL from "./mongo_user_dal.js";


class PLAYLIST_DAL{
        
        async getAllplaylists(){
            return await playlist_model.find();
        }
        async getPlaylistByID(id: string ) {
            return await playlist_model.findById(id);
        }
        async deleteByID(id: string){
            const playlist = await this.getPlaylistByID(id);
            for (const song of playlist.songs) {
                SONG_DAL.removePlaylistFromSong(song._id, id);
            }
            USER_DAL.removePlaylist(playlist.user, id);
            return await playlist_model.findByIdAndDelete(id);
        }
        async deleteSongFromPlaylist(playlist_id: string, song_id: string) {
            const playlist = await this.getPlaylistByID(playlist_id);
            const song_index = playlist.songs.findIndex((song:any)=>song._id === song_id);
            playlist.songs.splice(song_index,1);
            await playlist.songs.save();
        }

        async addSongToPlaylist(song_id: string, playlist_id: string){
            const playlist = await this.getPlaylistByID(playlist_id);
            const song = await SONG_DAL.getSongByID(song_id);
            playlist.songs.push(song);
            await playlist.save();
            return playlist;
    
        }
        async removeFromPlaylist(playlist_id: string, song_id: string){
            this.deleteSongFromPlaylist(playlist_id, song_id);
            SONG_DAL.removePlaylistFromSong(song_id, playlist_id);
        }
        // async create(data:ICreatePlaylistDto){
        //     const result = await playlist_model.create(data);
        //     if('songs' in data && data.songs.length > 0){
        //         const pending = data.songs.map(async (song_id: any) => {
        //             const song = await SONG_DAL.getSongByID(song_id);
        //             if(song){
        //                 song.playlists.push(result);
        //             await song.save();
        //             }
                    
        //         } )
        //         await Promise.all(pending);
        //     }
        //     return result;
        // }
        // async addSongToPlaylist(playlist_id: string, song_id: string){
        //     const playlist = await this.getPlaylistByID(playlist_id);
        //     const song = await SONG_DAL.getSongByID(song_id);
        //     playlist.songs.push(song);
        //     await playlist.save();
        //     return playlist;

        // }

        // async removeFromPlaylist(playlist_id: string, song_id: string){
        //     // const song = await song_model.findById(song_id);
        //     const song = await SONG_DAL.getSongByID(song_id);
        //     if(song){
        //         const playlist_index = song.playlists.findIndex((playlist:any)=>playlist._id === playlist_id);
        //         song.playlists.splice(playlist_index,1);
        //         await song.save();
        //     }
        //     const playlist = await this.getPlaylistByID(playlist_id);
        //     // const playlist = await playlist_model.findById(playlist_id);
        //     const song_index = playlist.songs.findIndex((song:any)=>song._id === song_id);
        //     playlist.songs.splice(song_index,1);
        //     await playlist.save();
        // }
        
        
}
export default new PLAYLIST_DAL();

