import { IUserSQL } from "../../modules/user/user.interface.js";
import { connection } from "../sql_db.js";
import { OkPacket } from "mysql2";

export async function createUser(data: IUserSQL) {
    const query = `INSERT INTO user(email, password,name,roles) VALUES(?,?,?,?)`;
    // console.log([Object.values(data)]);
    // console.log([...Object.values(data)]);
    // return 1;
    const params = Object.values(data);
    try{
        const [result,] = await connection.execute(query, params);
        const user = await getUserByID((result as OkPacket).insertId.toString());

        return user;

    }
    catch(err){
        throw new Error('error creating user');
    }
}

export async function createUser2(data: IUserSQL){
    const query = `INSERT INTO user SET ?`;
    const params = data;
    // return params;
    console.log('PARAMS:',params);
    try{
        const [result] = await connection.execute(query, params);
        return result;
        // const user = await getUserByID((result as OkPacket).insertId.toString());

        // return user;

    }
    catch(err){
        throw new Error(JSON.stringify({'message':'error creating user', 'error':err}));
    }
}

export async function getUserByID(id: string) {
    const query = "SELECT * FROM user WHERE id = ?";
    const params = [id];
    const [result,] = await connection.execute(query,params);
    return (result as unknown as IUserSQL[]);
}