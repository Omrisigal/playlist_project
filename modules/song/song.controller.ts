// import * as DB from "../../db/mongo_db.js";
import * as song_service2 from "./song.service.js";
import * as song_service from "./song.sql.service.js";
import * as playlist_service from "../playlist/playlist.sql.service.js";

import  {Response, Request, NextFunction} from 'express';
import { UrlError } from "../../error_messages.js";

export async function addSong (req: Request, res: Response){
    const user = await song_service.addSong(req.body);
    res.status(200).json(user);
}

// export async function getAllSongs2(req: Request, res: Response) {
//     const songs = await song_service.getAllSongs();
//     res.status(200).json(songs);
// }
export async function getAllSongs(req: Request, res: Response) {
    const songs = await  song_service.getAllSongs();
    console.log(songs);
    // const songs = await song_service.getAllSongs();
    res.status(200).json(songs);
}

export async function getSongByID(req: Request, res: Response){
    const song = await song_service.getSongByID(req.params.id);
    if (!song) {
        throw new UrlError('Url not found for request: '+req.baseUrl+req.url);
    }
    //return res.status(404).json({ status: "No user found." });
    res.status(200).json(song);
};

export async function removeSongFromPlaylist(req: Request, res: Response){
    console.log(req.params);
    const song = await song_service.removeSongFromPlaylist(req.params.id, req.params.playlist_id);
    console.log(song);
    const songs = await playlist_service.getPlaylistByID(req.params.playlist_id);
    console.log(songs);
    res.status(200).json(songs);
}

export async function approveSong(req: Request, res: Response) {
    const song = await song_service.approveSong(req.body.id, req.body.new_status);
    res.status(200).json(song);
}

export async function deleteSong(req: Request, res: Response) {
    const song = await song_service.deleteSong(req.params.id);
    res.status(200).json(song);}

// deleteSong
// export function someAdder(req: Request, res: Response) {
//     let num2 ='5';
//     if(num1== num1){
//         throw new Error('MY FUKING MESSAGE');
//     }
//     return 1;
// }


// export async function addSong (req: Request, res: Response){
//     const user = await DB.addSong(req.body);
//     res.status(200).json(user);
// }

// export async function getAllSongs(req: Request, res: Response) {
//     const songs = await DB.getAllSongs();
//     res.status(200).json(songs);
// }

// export async function getSongByID(req: Request, res: Response){
//     const song = await DB.getSongByID(req.params.id);
//     if (!song) {
//         throw new UrlError('Url not found for request: '+req.baseUrl+req.url);
//     }
//     //return res.status(404).json({ status: "No user found." });
//     res.status(200).json(song);
// };

// export async function removeSong(req: Request, res: Response){
//     const song = await DB.removeSong(req.params.id);
//     res.status(200).json(song);
// }
// export async function addToPlaylist(req: Request, res: Response){
//     const song = await DB.addToPlaylist(req.body.song_id, req.body.playlist_id);
//     res.status(200).json(song);
// };

