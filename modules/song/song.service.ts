import SONG_DAL from "../../db/mongo/mongo_song_dal.js";
import ARTIST_DAL from "../../db/mongo/mongo_artist_dal.js";
import PLAYLIST_DAL from "../../db/mongo/mongo_playlist_dal.js";
import { ICreateSongDto } from "./song.interface.js";
import { ObjectId } from "mongoose";



export async function getAllSongs(){
    const songs = await SONG_DAL.getAllSongs();
    return songs;
}
export async function getSongByID(id:string){
    const song = await SONG_DAL.getSongByID(id);
    return song;

}
export async function removeSong(song_id:string){
    const song = await SONG_DAL.deleteByID(song_id);
    return song;

}

export async function addSongToPlaylist(song_id:string, playlist_id: string){
    const song = await SONG_DAL.addSongToPlaylist(song_id, playlist_id);
    return song;
}

export async function addSong(data : ICreateSongDto) {
    const result = await SONG_DAL.create(data);
    return result;
}
