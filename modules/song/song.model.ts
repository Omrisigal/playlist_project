import mongoose, { Types } from 'mongoose'
const { Schema, model } = mongoose

export interface ISong {
    song_name:string,
    length: number,
    artist: Types.ObjectId,
    playlists: Types.ObjectId[]

}

export const SongSchema = new Schema<ISong>({
    song_name  :  { type : String, required : true },
    length     :  { type : Number, required : true },
    artist     :  { type: Schema.Types.ObjectId, ref:'artist'},
    playlists  :  [{ type: Schema.Types.ObjectId, ref:'playlist'}]
}, {timestamps:true});
  
export default model<ISong>('song',SongSchema);