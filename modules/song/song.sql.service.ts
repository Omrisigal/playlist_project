import SONG_DAL from "../../db/mongo/mongo_song_dal.js";
import ARTIST_DAL from "../../db/mongo/mongo_artist_dal.js";
import PLAYLIST_DAL from "../../db/mongo/mongo_playlist_dal.js";
import { ICreateSongDto ,ISongDtoSQL} from "./song.interface.js";
import { ObjectId } from "mongoose";
// import connection from "../../db/sql_db.js";
import {connection , config} from "../../db/sql_db.js";
import mysql from 'mysql2/promise';
import * as playlist_service from "../playlist/playlist.sql.service.js";
// import { RowDataPacket } from "mysql2";
import {createConnection, OkPacket, QueryError, RowDataPacket} from 'mysql2';
import { resourceLimits } from "worker_threads";

// const connection = createConnection(process.env['DB']);


interface IRole extends RowDataPacket {
  id: string,
  role_name: string
}

export async function getAllSongs(){
    const [result,] = await connection.execute(
      'SELECT * FROM song s WHERE s.status LIKE \'APPROVED\'');
    return result;
}


export async function approveSong(song_id: string, status_id: string) {
  const query = `UPDATE song SET status = ?
                  WHERE id = ?`; 
  const params = [status_id, song_id];
  const [result,] = await connection.execute(query, params);
  const song = await getSongByID(song_id);
  return song;
}

export async function getSongByID(id:string){
    const query = `SELECT * FROM song WHERE id = ${id}`; 
    const params = [id];
    const [result,] = await connection.execute(query, params);
    return result;

}
export async function removeSongFromPlaylist(song_id:string, playlist_id: string){
    const query = `DELETE FROM playlist_songs WHERE song_id = ? AND playlist_id= ?`;
    const params = [song_id,playlist_id];
    const [result,] = await connection.execute(query, params);
    return result;
}

export async function addSongToPlaylist(song_id:string, playlist_id: string){
  const query = `INSERT INTO playlist_songs(song_id,playlist_id) VALUES(?,?)`;
  const params = [song_id, playlist_id];
  const [result,] = await connection.execute(query, params);
  const playlist = await playlist_service.getPlaylistByID(playlist_id);
  return playlist;
  
}

export async function addSong(data : ISongDtoSQL) {
    const query = `INSERT INTO song (artist_id,name,status) VALUES (?,?,'PENDING')`;
    const params = [data.artist_id, data.name];
    try{
      const [result,] = await connection.execute(query, params);
      const song = await getSongByID((result as OkPacket).insertId.toString());
      return song;
    }
    catch(err){
      throw new Error('Error creating song');
    }
    
}

export async function deleteSong(song_id:string){
  let query = `DELETE FROM playlist_songs WHERE song_id = ${song_id}`;
  let params = [song_id];
  let [result,] = await connection.execute(query, params);

  query = `DELETE FROM song WHERE id = ${song_id}`;
  params = [song_id];
  [result,] = await connection.execute(query, params);
  return result;
}