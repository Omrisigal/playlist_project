interface ISongDto {
    _id: string;
    song_name: string;
    artist: string;
    length: string;
    playlists: string[]
  }

  export type ICreateSongDto = Omit<ISongDto, '_id'>;
  
  export type IUpdateSongDto = Partial<ICreateSongDto>;
  
  export interface ISongDtoSQL {
    name: string;
    artist_id: string;
    status: string;
  }