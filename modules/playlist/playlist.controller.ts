import * as DB from "../../db/mongo/mongo_user_dal.js";
import  {Response, Request, NextFunction} from 'express';
import { UrlError } from "../../error_messages.js";
import * as playlist_service2 from "./playlist.service.js";
import * as playlist_service from "./playlist.sql.service.js";



export async function getAllPlaylists(req: Request, res: Response) {
    const playlists = await playlist_service.getAllPlaylists();
    res.status(200).json(playlists);
}

export async function getPlaylistByID(req: Request, res: Response){
    const songs = await playlist_service.getPlaylistByID(req.params.id);
    if (!songs) {
        throw new UrlError('Url not found for request: '+req.baseUrl+req.url);
    }
    //return res.status(404).json({ status: "No user found." });
    res.status(200).json(songs);
};




