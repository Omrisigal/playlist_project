import SONG_DAL from "../../db/mongo/mongo_song_dal.js";
import ARTIST_DAL from "../../db/mongo/mongo_artist_dal.js";
import PLAYLIST_DAL from "../../db/mongo/mongo_playlist_dal.js";
import { SongSchema } from "../song/song.model.js";
import { IPlaylist } from "./playlist.model.js";
import { ICreatePlaylistDto,IPlaylistDtoSQL } from "./playlist.interface.js";
import { ObjectId } from "mongoose";
import mongo_user_dal from "../../db/mongo/mongo_user_dal.js";
import {connection , config} from "../../db/sql_db.js";


export async function getAllPlaylists(){
    const query = `SELECT p.id AS \`playlist id\`, p.name AS \`playlist name\`, s.name AS \` song name\` 
                FROM playlist_songs ps
                RIGHT JOIN song s ON ps.song_id = s.id
                RIGHT JOIN playlist p ON p.id = ps.playlist_id`; 
    const [result] = await connection.execute(query);
    return result;
    
}
export async function getPlaylistByID(id:string){
     throw new Error(" CUSTOM message error");
    const query = `SELECT p.id AS \`playlist id\`, p.name AS \`playlist name\`, s.name AS \` song name\` 
    FROM playlist_songs ps
    RIGHT JOIN song s ON ps.song_id = s.id
    RIGHT JOIN playlist p ON p.id = ps.playlist_id 
    WHERE p.id = ${id}`; 
    const params = [id];
    const [result,] = await connection.execute(query, params);
    return result;
}


// export async function removeFromPlaylist(playlist_id:string, song_id :string){
//     const playlist = await PLAYLIST_DAL.removeFromPlaylist(playlist_id,song_id);
//     return playlist;

// }

// export async function createPlaylist(data : ICreatePlaylistDto) {
//     const result = await PLAYLIST_DAL.create(data);
//     return result;

// }