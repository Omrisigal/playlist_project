/* 
  if there is an error thrown in the DB, asyncMiddleware
  will pass it to next() and express will handle the error */
  import raw from "../../middleware/route.async.wrapper.js";
  import {getAllPlaylists,getPlaylistByID} from "./playlist.controller.js";
  import express, {Request,Response, NextFunction } from 'express';
  
  const router = express.Router();

  
  // parse json req.body on post routes
  router.use(express.json())
  router.use((req:Request, res:Response, next:NextFunction)=>{
    req.id = Math.random().toString(36).substring(7);
    next();
  })
 


  // GET ALL PLAYLISTS
  router.get("/", raw(getAllPlaylists));
  
  // GETS A SINGLE PLAYLIST
  router.get("/:id", raw(getPlaylistByID));



  
  export default router;
  