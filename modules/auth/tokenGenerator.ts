import jwt from 'jsonwebtoken'
const {CLIENT_ORIGIN, APP_SECRET = "supercalifragilisticexpialidocious", ACCESS_TOKEN_EXPIRATION, REFRESH_TOKEN_EXPIRATION} = process.env;


export function generateAccessT(payload:any) {
    const access_token = jwt.sign(payload, APP_SECRET, {
        expiresIn: ACCESS_TOKEN_EXPIRATION // expires in 1 minute
      })
    return access_token;
}

export function generateRefreshT(payload:any) {
    const refresh_token = jwt.sign(payload, APP_SECRET, {
        expiresIn: REFRESH_TOKEN_EXPIRATION // expires in 1 minute
      })
    return refresh_token;
}

export function generateTokens(accessTokenPayload: any, RefreshTokenPayload: any ){
    
    return  {
        access_token: generateAccessT(accessTokenPayload),
        refresh_token: generateRefreshT(RefreshTokenPayload)
    };

}