/* 
  if there is an error thrown in the DB, asyncMiddleware
  will pass it to next() and express will handle the error */
  import raw from "../../middleware/route.async.wrapper.js";
  import {loginUser, getAccessToken, logout} from "./auth.controller.js";
  import express, {Request,Response, NextFunction } from 'express';
  import cookieParser from 'cookie-parser'

  const router = express.Router();
  router.use(cookieParser())

  
  // parse json req.body on post routes
  router.use(express.json())
  
  // login
  router.post("/login", raw(loginUser));
  router.get("/get-access-token", raw(getAccessToken));
  router.put("/logout", raw(logout));


  
 



  
  export default router;
  