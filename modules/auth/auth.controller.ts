import * as user_service2 from "../user/user.service.js";
import bcrypt from "bcryptjs";
import  {Response, Request, NextFunction} from 'express';
import * as Token from "./tokenGenerator.js";
import ms from 'ms';
import jwt, { JwtPayload } from 'jsonwebtoken';
import * as user_service from "../user/user.sql.service.js";
import { IUserSQL } from "../user/user.interface.js";

const {APP_SECRET = "supercalifragilisticexpialidocious"} = process.env;

interface loginUserCrt {
    email: string,
    password: string
}

export async function loginUser(req: Request, res: Response) {
    const requested_user : loginUserCrt = req.body;
    let user:any;
    try{
        user = await  user_service.getUserByEmail(requested_user.email) as any;
    }
    catch(err){
        throw new Error('error loggin in');
    }
    user =  user[0];
    if(!user) return null;
    const auth = bcrypt.compare(user.password, requested_user.password);
    if(!auth) return null;
    user.refresh_token = "";
    const tokens = Token.generateTokens({user_id:user.id, user_roles:user.roles}, {user_id:user.id, user_roles:user.roles});
    const added_user = user_service.addRefreshToken(user.id, tokens.refresh_token);
    res.cookie('refresh_token',tokens.refresh_token, {
        maxAge: ms('60d'), //60 days
        httpOnly: true
      })
    res.status(200).json(tokens.access_token);
}

// export async function loginUser2(req: Request, res: Response) {
//     const requested_user : loginUserCrt = req.body;
//     const user = await  user_service.getUserByEmail(requested_user.email);
//     if(!user) return null;
//     const auth = bcrypt.compare(user.password, requested_user.password);
//     if(!auth) return null;
//     const tokens = Token.generateTokens({user:user}, {user:user});
//     const added_user = user_service.addRefreshToken(user.id, tokens.refresh_token);
//     res.cookie('refresh_token',tokens.refresh_token, {
//         maxAge: ms('60d'), //60 days
//         httpOnly: true
//       })
//     res.status(200).json(tokens.access_token);
// }

export async function getAccessToken(req: Request, res: Response) {
    const {refresh_token} = req.cookies;

    if(!refresh_token) return null;
    const decoded = await jwt.verify(refresh_token, APP_SECRET)
    console.log({decoded})

    //check user refresh token in DB
    const {user_id} = decoded;
    const user = await  user_service.getUserByID(user_id);
    // res.status(200).json(user);
    if(!(user[0].refresh_token)) return null;

    if (user[0].refresh_token!=refresh_token) return null;

    const access = Token.generateAccessT({user_id: user_id, user_roles:user[0].roles});
    res.status(200).json(access);

}

export async function logout(req: Request, res: Response) {
    const deleted = await user_service.deleteRefreshForUser(req.body.id);
    if(deleted){
        res.status(200).json("logged out");
    }
    else{
        res.status(200).json("user not found");

    }
}



