export interface IUserMongo {
    first_name: string;
    last_name: string;
    email: string;
    phone: string;
    playlists: string[]
  }

  export interface IUserSQL {
    id?:string;
    password: string;
    name: string;
    email: string;
    roles: string;
    refresh_token?: string;
  }

  export type IUserOptionalSQL = Partial<IUserSQL>;
   