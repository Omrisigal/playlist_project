import SONG_DAL from "../../db/mongo/mongo_song_dal.js";
import ARTIST_DAL from "../../db/mongo/mongo_artist_dal.js";
import PLAYLIST_DAL from "../../db/mongo/mongo_playlist_dal.js";
import { ObjectId } from "mongoose";
import { ICreatePlaylistDto } from "../playlist/playlist.interface.js";
import USER_DAL from "../../db/mongo/mongo_user_dal.js";
import {IUser} from "./user.model.js";

export async function create(data: IUser) {
    USER_DAL.createUser(data);
}

export async function removeFromPlaylist(playlist_id:string, song_id :string){
    const playlist = await PLAYLIST_DAL.removeFromPlaylist(playlist_id,song_id);
    return playlist;

}

export async function createPlaylist(data : ICreatePlaylistDto) {
    const result = await USER_DAL.createPlaylist(data);
    return result;
}

export async function getAllUsers(selectors: string[], page?: number, itemsInPage?: number){
    return await USER_DAL.getAllUsers(selectors, page, itemsInPage);
}

export async function getUserByID(id: string) {
    return await USER_DAL.getUserByID(id);
}
export async function deleteUserByID(id: string) {
    return await USER_DAL.deleteUserByID(id);
}

export async function updateUser(id: string , data: IUser) {
    return await USER_DAL.updateUser(id, data);
}

export async function addSongToPlaylist(song_id: string,playlist_id: string) {
    return await PLAYLIST_DAL.addSongToPlaylist(song_id, playlist_id);
}

export async function getUserByEmail(email : string) {
    const user = USER_DAL.getUserByEmail(email);
    return user;
}

export async function deleteRefreshForUser(id: string) {
    const user = USER_DAL.deleteRefreshToken(id);
    return user;
}

export async function addRefreshToken(id: string, token: string) {
    const user = await USER_DAL.addRefreshTokenToDB(id, token);
    return user;
}