import mongoose, { Types } from 'mongoose';
const { Schema, model } = mongoose;

const UserSchema = new Schema({
    first_name  : String,
    last_name   : String,
    email       : String,
    phone       : String,
    playlists: [{ type: Schema.Types.ObjectId, ref:'playlist'}],
    password: String,
    refresh_token: String
});
  
export default model('user',UserSchema);

export interface IUser {
    first_name  : string,
    last_name   : string,
    email       : string,
    phone       : string,
    playlists: Types.ObjectId[],
    password: string,
    refresh_token: string,
    roles : string
}