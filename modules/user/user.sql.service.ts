
import { IPlaylistDtoSQL } from "../playlist/playlist.interface.js";
import {IUserOptionalSQL, IUserSQL} from "./user.interface.js";
import {connection , config} from "../../db/sql_db.js";
import { OkPacket } from "mysql2"; 
import * as playlist_service from "../playlist/playlist.sql.service.js";
import * as SQL_DAL from "../../db/sql/user_sql_dal.js";

export async function create(data: IUserSQL) {
    const user = await SQL_DAL.createUser(data);
    return user;
    
}
// export async function create(data: IUserSQL) {
//     const query = `INSERT INTO user(email, password,name,roles) VALUES(?,?,?,?)`;
//     const params = [data.email, data.password,data.name, data.roles ];
//     try{
//         const [result,] = await connection.execute(query, params);
//         const user = await getUserByID((result as OkPacket).insertId.toString());

//         return user;

//     }
//     catch(err){
//         throw new Error('error creating user');
//     }
// }

export async function removeFromPlaylist(playlist_id:string, song_id :string){
    const query = `DELETE FROM playlist_songs WHERE song_id =? AND playlist_id=?`;
    const params = [song_id, playlist_id];
    const [result,] = await connection.execute(query, params);
    return result;

}

export async function createPlaylist(data : IPlaylistDtoSQL) {
    const query = `INSERT INTO playlist(name) VALUES(?)`;
    const params = [data.name];
    try{
        const [result,] = await connection.execute(query, params);
        const playlist = await playlist_service.getPlaylistByID((result as OkPacket).insertId.toString());
        return playlist;
    }
    catch(err){
        throw new Error('error creating playlist');
    }
   
}

export async function getAllUsers(){
    const query = `SELECT * FROM user`;
    const [result,] = await connection.execute(query);
    return result;
}

export async function getUserByID(id: string) {
    const query = "SELECT * FROM user WHERE id = ?";
    const params = [id];
    const [result,] = await connection.execute(query,params);
    return (result as unknown as IUserSQL[]);
}

export async function deleteUserByID(id: string) {
    const query = `DELETE FROM user WHERE id =? `;
    const params = [id];
    const [result,] = await connection.execute(query, params);
    return {"message": "success, user id "+id+ " was deleted"};
}
interface IUserIndex<IUserOptionalSQL> {
    [key: string]: string;
}
export async function updateUser(id: string, user: IUserIndex<IUserOptionalSQL>) {
    
    let query = "UPDATE user SET ";
    let updates = "";
    for (const key in user ) {
        updates+= key+"='"+user[key]+"' ,";
    }
    query+=updates.slice(0,-1);
    query+=" WHERE id = ?";
    console.log(query);
    const params = [ id ];
    const [result,] = await connection.execute(query, params);
    const updated_user = await getUserByID(id);
    return updated_user;
}

export async function addSongToPlaylist(song_id: string,playlist_id: string) {
    const query = `INSERT INTO playlist_songs(song_id, playlist_id) VALUES(?,?)`;
    const params = [song_id, playlist_id];
    const [result,] = await connection.execute(query, params);
    const playlist = await playlist_service.getPlaylistByID(playlist_id);
    return playlist;
}

export async function  getUserByEmail(email : string) {
    const query = "SELECT * FROM user u WHERE u.email = ?";
    const params = [email ];
    const [result,] = await connection.execute(query, params);
    return result as unknown as IUserSQL;
}

export async function deleteRefreshForUser(id: string) {
    const query = "UPDATE user u SET refresh_token = '' WHERE u.id = ?";
    const params = [ id ];
    const [result,] = await connection.execute(query, params);
    return result;
}

export async function addRefreshToken(id: string, token: string) {
    const query = "UPDATE user u SET refresh_token = ? WHERE u.id = ?";
    const params = [token, id ];
    const [result,] = await connection.execute(query, params);
    return result;
}