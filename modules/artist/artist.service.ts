import SONG_DAL from "../../db/mongo/mongo_song_dal.js";
import ARTIST_DAL from "../../db/mongo/mongo_artist_dal.js";
import PLAYLIST_DAL from "../../db/mongo/mongo_playlist_dal.js";
import { IArtist } from "./artist.model.js";
import { ICreateArtistDto } from "./artist.interface.js";
import { ObjectId } from "mongoose";


export async function getArtistByID(id:string){
    const artist = await ARTIST_DAL.getArtistByID(id);
    return artist;
}

export async function getAllSongsOfArtist(id:string){
    // const songs = await ARTIST_DAL.getArtistSongsByID(id);
    const artist = await ARTIST_DAL.getArtistByID(id);
    //  const artist = await ARTIST_DAL.getArtistByID(id);
    //  console.log(artist);
    //  const songs = artist.populate('songs');
      console.log(artist.songs);
     return artist.songs;
    // return 1;
}

export async function getAllArtists(){
    const artists = await ARTIST_DAL.getAllArtists();
    return artists;
}

export async function removeArtist(artist_id: string) {
    //remove songs(include remove from playlist)
    const artist = await ARTIST_DAL.deleteByID(artist_id);
    return artist;
}

export async function addArtist(data : ICreateArtistDto) {
    const result = await ARTIST_DAL.create(data);
    return result;
}