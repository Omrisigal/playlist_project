/* 
  if there is an error thrown in the DB, asyncMiddleware
  will pass it to next() and express will handle the error */
  import raw from "../../middleware/route.async.wrapper.js";
  // import {getAllArtists, getAllSongsOfArtist, getArtistByID, addArtist, deleteArtist, approveArtist} from "./artist.controller.js";
  import {getAllArtists, getAllSongsOfArtist, getArtistByID, addArtist, deleteArtist,approveArtist} from "./artist.controller.js";
import { authenticate, authorize } from "../../middleware/authenticate.middleware.js";
  import express, {Request,Response, NextFunction } from 'express';
import { addReqID } from "../../middleware/addReqID.middleware.js";
  import { ROLE } from "../../constants.js";
  const router = express.Router();

  
  // parse json req.body on post routes
  router.use(express.json())
  router.use(addReqID);

  // CREATES A NEW ARTIST - POST METHOD
  router.post("/", raw(addArtist));
  
  // GET ALL SONGS OF ARTIST
  router.get("/:id/songs", raw(getAllSongsOfArtist));
  
  // GETS A SINGLE ARTIST
  router.get("/:id", raw(getArtistByID));

  // GETS ALL ARTISTS
  router.get("/", raw(getAllArtists));

  router.delete("/:id",raw(deleteArtist));
    
  // router.put("/approve_artist", raw(approveArtist));
  router.put("/approve_artist", authenticate, await authorize([ROLE.MODERATOR,ROLE.ADMIN]),raw(approveArtist));

    

  export default router;
  