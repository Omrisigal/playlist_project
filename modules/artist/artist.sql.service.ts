import SONG_DAL from "../../db/mongo/mongo_song_dal.js";
import ARTIST_DAL from "../../db/mongo/mongo_artist_dal.js";
import PLAYLIST_DAL from "../../db/mongo/mongo_playlist_dal.js";
import { IArtist } from "./artist.model.js";
import { ICreateArtistDto ,IArtistDtoSQL} from "./artist.interface.js";
import { ObjectId } from "mongoose";
import {connection , config} from "../../db/sql_db.js";
import {createConnection, OkPacket, QueryError, RowDataPacket} from 'mysql2';


export async function getArtistByID(id:string){
    const query = 'SELECT a.id,a.name , s.name AS \'song_name\' FROM artist a  \
                        LEFT JOIN song s ON a.id = s.artist_id  AND s.status LIKE \'APPROVED\' \
                        WHERE a.id = ? AND a.status LIKE \'APPROVED\' '; 
    const params = [id];
    const [result,] = await connection.execute(query,params);
    return result;
   
}

export async function getAllSongsOfArtist(id:string){
    const query = 'SELECT a.id,a.name , s.name AS \'song_name\' FROM artist a  \
                    INNER JOIN song s ON a.id = s.artist_id  AND s.status LIKE \'APPROVED\' \
                    WHERE a.id = ? AND a.status LIKE \'APPROVED\''; 
    const params = [id];
    const [result,] = await connection.execute(query, params);
    return result;
}

export async function getAllArtists(){
    const query = 'SELECT a.id,a.name , s.name AS \'song_name\' FROM artist a  \
                LEFT JOIN song s ON a.id = s.artist_id  AND s.status LIKE \'APPROVED\' \
                WHERE  a.status LIKE \'APPROVED\'' ; 
    const [result,] = await connection.execute(query);
    return result;
}

export async function removeArtist(artist_id: string) {
    await connection.beginTransaction();
    try{
        let query = `DELETE FROM song WHERE artist_id = ${artist_id}`;
        let params = [artist_id];
        let [result,] = await connection.execute(query, params);
    
        query = `DELETE FROM playlist_songs ps WHERE song_id IN 
                (SELECT id FROM     
                        (SELECT song_id FROM song WHERE artist_id = ?) n)`;
        params = [artist_id];
        [result,] = await connection.execute(query, params);
    
        query = `DELETE FROM artist WHERE id = ${artist_id}`;
        params = [artist_id];
        [result,] = await connection.execute(query, params);
        await connection.commit();
        return result;
    }
    catch(err){
        connection.rollback();
    }
    

}

export async function addArtist(data : IArtistDtoSQL) {
    let query = `INSERT INTO artist (name,status) VALUES (?,'PENDING')`;
    let params = [data.name];
    let [result,] = await connection.execute(query, params);
    query = 'SELECT * FROM artist a WHERE a.id = ?  '; 
    const new_artist_id = (result as OkPacket).insertId.toString();
    params = [new_artist_id];
    [result,] = await connection.execute(query, params);
    return result;   
}

export async function approveArtist(artist_id: string, status_id: string) {
    const query = `UPDATE artist SET status= ?
                    WHERE id = ?`; 
    const params = [status_id, artist_id];
    const [result,] = await connection.execute(query, params);
    return result;
  }