// import { User } from "./db/users.model.js";

import { IUser } from "./modules/user/user.model.js";

export declare interface user{
  id: string;
 
}

export declare global {

namespace Express {
    interface Request {
        id: string,
        user_id:string,
        user_roles: string
    }
  }
}
